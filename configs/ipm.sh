#!/bin/sh
#
# IPM
# http://ipm-hpc.org

source ./helper.sh
set_stage_dl https://github.com/nerscadmin/IPM.git
module load openmpi-1.6.5

patch -p0 < $C4_PKG/patches/ipm.patch
set +e
libtoolize
aclocal
automake --add-missing
autoreconf
set -e

./configure --prefix=$PREFIX/IPM
make install

leave_stage
