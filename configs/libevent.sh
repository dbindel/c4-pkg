#/bin/sh
#
# livevent -- an event notification library
# http://libevent.org/

source ./helper.sh
stage_dl_ac https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz
