#!/bin/sh
#
# ARPACK-ng -- Next generation ARPACK
# http://forge.scilab.org/index.php/p/arpack-ng/

source ./helper.sh
stage_dl_ac http://forge.scilab.org/index.php/p/arpack-ng/downloads/get/arpack-ng-3.1.3.tar.gz
